#include <ros/ros.h>
#include <std_msgs/String.h>
#include "geometry_msgs/Twist.h"
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>	
#include <string.h>		/* String function definitions */

#include <roomba_remote_control/hello_world.h>
#include <roomba_remote_control/roombalib.h>

// declare shared varaible of the main loop and the functions
int velocity = 0;
int waitmillis = 1000;
bool track = false;
Roomba* roomba = NULL;

// initial function of roomba
void init() {
	roomba = roomba_init("/dev/ttyO4");
	if( roomba == NULL )
		fprintf(stderr, "error: couldn't open roomba\n");
    else
        say_hello();
}

// the function to run when receving texutal messages
void cmd_run(const std_msgs::String::ConstPtr& msg) {
	ROS_INFO("cmd received: [%s]", msg->data.c_str());

	// send commands according to the textual message
	switch (msg->data.at(0)) {
	case 0:
		break;
	    case 'i':
		init();
		break;
		case 't':
		track = true;
		break;
		case '1':
		roomba_drive( roomba, 200, 700 );
		break;
		case '2':
		roomba_drive( roomba, 200, -700 );
		break;
		case 'm':
		if(roomba) roomba_stop( roomba );
		track = false;
		break;
	    case 'c':
		if(roomba) roomba_close(roomba);
		break;
	    case 'f':
		if(roomba) roomba_forward( roomba );
		break;
	    case 'b':
		if(roomba) roomba_backward( roomba );
		break;
	    case 'l':
		if(roomba) roomba_spinleft( roomba );
		break;
	    case 'r':
		if(roomba) roomba_spinright( roomba );
		break;
	    case 's':
		if(roomba) roomba_stop( roomba );
		break;
	    case 'w':
		waitmillis = strtol(optarg,NULL,10);
		roomba_wait( waitmillis );
		break;
	    case 'V':
		velocity = strtol(optarg,NULL,10);
		if(roomba) roomba_set_velocity( roomba, velocity );
		break;
	    case 'S':
		if(roomba) {
		    roomba_read_sensors(roomba);
		    roomba_print_sensors(roomba);
		}
		break;
	    case 'R':
		if(roomba) {
		    roomba_read_sensors(roomba);
		    roomba_print_raw_sensors(roomba);
		}
	    case 'D':
		roombadebug++;
	    case '?':
		break;
    }
}

// the function to run when receving velocity messages
void cmd_vel_run(const geometry_msgs::Twist msg) {
	// run only roomba is on and it's the tracking mode
    if(roomba && track)
    {
    	// rescale the linear velocity
        int velocity = msg.linear.x * 133;

        // apply an exponential function to angular velocity
        float r = - msg.angular.z;
        int radius;
		if (r > 0)
		{
			radius = 2000 * (float)exp(-0.7 * r);
		}
		else
		{
			radius = -2000 * (float)exp(0.7 * r);
		}
    
    	// send the command
	    roomba_drive( roomba, velocity, radius );

	    // output used data
		ROS_INFO_STREAM("Camera parameters:");
		ROS_INFO_STREAM(velocity);
		ROS_INFO_STREAM(radius);
	
    }
}

int main(int argc, char** argv) {
	// roomba initialisation
	init();

	// ROS initialisation
	ros::init(argc, argv, "roomba");
	ros::NodeHandle nh;	

	// subscriber settings
	ros::Subscriber sub_cmd = nh.subscribe("/roomba/cmd", 1000, cmd_run);
    ros::Subscriber sub_cmd_vel = nh.subscribe("/roomba/cmd_vel", 3000, cmd_vel_run);

	// Debug code
	ROS_INFO_STREAM("Hello, ROS!");

	// loop rate setting
	ros::Rate loop_rate(10);

	// set a global parameter
	nh.setParam("track", true);

	// ROS loop
	while (ros::ok)
	{
		// get the global parameter
		bool track_temp;
		nh.getParam("track", track_temp);

		// update the global parameter
		if (track_temp != track)
		{
			nh.setParam("track", track);
			ROS_INFO_STREAM(track);
		}

		// run node once
		ros::spinOnce();

		// sleep according to the loop rate
	    loop_rate.sleep();
	}

	// run node
	ros::spin();

	// close serial connection
    if (roomba) roomba_close(roomba);

	return 0;
}
