#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include <sstream>
#include <iostream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cmath>    /* exponential function */

using namespace cv;
using namespace std;

int main(int argc, char **argv)
{
  // ROS initialisation
  ros::init(argc, argv, "camera");
  ros::NodeHandle n;

  // publisher setting
  ros::Publisher camera_pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1000); 

  // loop rate setting
  ros::Rate loop_rate(10);

  // cv_bridge setting
  image_transport::ImageTransport it(n);
  image_transport::Publisher pub = it.advertise("camera/image", 1);

  // opencv
  VideoCapture cap(0); //capture the video from webcam

    if ( !cap.isOpened() )  // if not success, exit program
    {
         cout << "Cannot open the web cam" << endl;
         return -1;
    }
  // set the video size
  // this size cannot be too large  
  // due to the power of BBB
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
  cap.set(CV_CAP_PROP_FPS, 10);

  // declartion of HSV and position variables
  int iLowH, iHighH, iLowS, iHighS, iLowV, iHighV, count, iLastX, iLastY;

  // some color values
  // use green now
  bool nav_blue = false;    
  if (nav_blue)
  {
  	iLowH = 96;
  	iHighH = 112;

  	iLowS = 72; 
  	iHighS = 145;

  	iLowV = 110;
  	iHighV = 243;

    count = 0;

    iLastX = -1; 
    iLastY = -1;
  }
  bool green = true;
  if (green)
  {
    iLowH = 20;
    iHighH = 60;

    iLowS = 50; 
    iHighS = 150;

    iLowV = 50;
    iHighV = 255;

    count = 0;

    iLastX = -1; 
    iLastY = -1;
  }

  // capture an image from the camera
  Mat imgTmp;
  cap.read(imgTmp); 

  // declare an empty image with the same size
  Mat imgLines = Mat::zeros( imgTmp.size(), CV_8UC3 );;

  // declare a frame image
  cv::Mat frame;

  // delcare the sensor message 
  sensor_msgs::ImagePtr msg;

  // declare a variable to indicate if in the tracking mode
  bool track;
  
  // main loop of ROS
  while (ros::ok())
  {
    // execute if it is in the tracking mode
    n.getParam("track", track);
    if (track)
    {
      // declare a variable to store the frame of the original video
      Mat imgOriginal;

      // read a new frame from video
      bool bSuccess = cap.read(imgOriginal); 

      //if not success, break loop
       if (!bSuccess) 
      {
           cout << "Cannot read a frame from video stream" << endl;
           break;
      }

      // declare a variable to store the HSV image 
      Mat imgHSV;

      // convert the image from BGR to HSV
      cvtColor(imgOriginal, imgHSV, COLOR_BGR2HSV); 

      // declare a variable to store the thresholded image 
      Mat imgThresholded;

      // threshold accroding to the HSV values
      inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);
            
      // noise reduction (small blocks)
      erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
      dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 

      // noise reduction (small holes) 
      dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 
      erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

      // calculate the moments
      Moments oMoments = moments(imgThresholded);

      // get the required data to calculate the centroid
      double dM01 = oMoments.m01;
      double dM10 = oMoments.m10;
      double dArea = oMoments.m00;

      // declare and initilise the velocity message
      geometry_msgs::Twist msg;
      msg.linear.x = 0;
      msg.angular.z = 0;

      // work only there is an object and it is not too large
      if (dArea > 10000 && dArea < 2e+6)
      {

        //calculate the position of the object
        int posX = dM10 / dArea;
        int posY = dM01 / dArea;        
        
        // convert the postion to the velocity
        msg.linear.x = 1 + (1 - 1 / (exp((float)-dArea / 10000000) + 1));
        float dev = 1.5 * abs((float)(posX - imgTmp.size().width / 2) / (float)(imgTmp.size().width / 2));

        // re-sign the angluar velocity accroding to the position
        if (posX <= imgTmp.size().width / 2)
        {
          //cout << "right" << endl; // DEBUG code
          msg.angular.z = -dev;
        }
        else
        {
          //cout << "left" << endl; // DEBUG code
          msg.angular.z = dev;
        }

        // DEBUG code
        //cout << exp(dArea / 100000) << endl;
        //cout << msg.linear.x << endl;
        //cout << dArea << endl;
        //cout << dev << endl;

        // update the position
        iLastX = posX;
        iLastY = posY;
      }

      // publish motion message	  
      camera_pub.publish(msg);

      // DEBUG code
      //ROS_INFO_STREAM("msg published");

    }

    // publish image message
    cap >> frame; 

    // check if video stream frame is empty
    if(!frame.empty()) {
          pub.publish(cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg());
    }

    // run node once
    ros::spinOnce();

    // sleep according to the loop rate
    loop_rate.sleep();
  }

  return 0;
}
