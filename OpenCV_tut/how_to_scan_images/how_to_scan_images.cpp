#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

Mat& ScanImageAndReduceIterator(Mat& I, const uchar* const table)
{
    // accept only char type matrices
    CV_Assert(I.depth() != sizeof(uchar));

    const int channels = I.channels();
    switch(channels)
    {
    case 1:
        {
            MatIterator_<uchar> it, end;
            for( it = I.begin<uchar>(), end = I.end<uchar>(); it != end; ++it)
                *it = table[*it];
            break;
        }
    case 3:
        {
            MatIterator_<Vec3b> it, end;
            for( it = I.begin<Vec3b>(), end = I.end<Vec3b>(); it != end; ++it)
            {
                (*it)[0] = table[(*it)[0]];
                (*it)[1] = table[(*it)[1]];
                (*it)[2] = table[(*it)[2]];
            }
        }
    }

    return I;
}

int main(int argc, char** argv )
{
	Mat I, O;

	// input image
	
	I = imread( argv[1], 1 );

	if ( !I.data )
	{
		printf("No image data \n");
		return -1;
	}

	// output the original image

	namedWindow("Original Image", CV_WINDOW_AUTOSIZE );
	imshow("Original Image", I);

	// time measurement

	double t = (double)getTickCount();

	// generate lookup table

	// convert our input string to number - C++ style
	int divideWith = 0; 
	stringstream s;
	s << argv[2];
	s >> divideWith;

	if (!s || !divideWith)
	{
	    cout << "Invalid number entered for dividing. " << endl;
	    return -1;
	}

	uchar table[256];
	for (int i = 0; i < 256; ++i)
	   table[i] = (uchar)(divideWith * (i/divideWith));

	// processing

	O = ScanImageAndReduceIterator(I, table);

	// output time measurement

	t = ((double)getTickCount() - t)/getTickFrequency();
	cout << "Times passed in seconds: " << t << endl;

	// output the processed image

	imshow("Processed Image", O);

	waitKey(0);

	return 0;
}
