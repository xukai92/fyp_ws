# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/kai/OpenCV_tut/discrete_fourier_transform

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/kai/OpenCV_tut/discrete_fourier_transform

# Include any dependencies generated for this target.
include CMakeFiles/discrete_fourier_transform.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/discrete_fourier_transform.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/discrete_fourier_transform.dir/flags.make

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o: CMakeFiles/discrete_fourier_transform.dir/flags.make
CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o: discrete_fourier_transform.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/kai/OpenCV_tut/discrete_fourier_transform/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o -c /home/kai/OpenCV_tut/discrete_fourier_transform/discrete_fourier_transform.cpp

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/kai/OpenCV_tut/discrete_fourier_transform/discrete_fourier_transform.cpp > CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.i

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/kai/OpenCV_tut/discrete_fourier_transform/discrete_fourier_transform.cpp -o CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.s

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.requires:
.PHONY : CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.requires

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.provides: CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.requires
	$(MAKE) -f CMakeFiles/discrete_fourier_transform.dir/build.make CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.provides.build
.PHONY : CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.provides

CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.provides.build: CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o

# Object files for target discrete_fourier_transform
discrete_fourier_transform_OBJECTS = \
"CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o"

# External object files for target discrete_fourier_transform
discrete_fourier_transform_EXTERNAL_OBJECTS =

discrete_fourier_transform: CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o
discrete_fourier_transform: CMakeFiles/discrete_fourier_transform.dir/build.make
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
discrete_fourier_transform: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
discrete_fourier_transform: CMakeFiles/discrete_fourier_transform.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable discrete_fourier_transform"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/discrete_fourier_transform.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/discrete_fourier_transform.dir/build: discrete_fourier_transform
.PHONY : CMakeFiles/discrete_fourier_transform.dir/build

CMakeFiles/discrete_fourier_transform.dir/requires: CMakeFiles/discrete_fourier_transform.dir/discrete_fourier_transform.cpp.o.requires
.PHONY : CMakeFiles/discrete_fourier_transform.dir/requires

CMakeFiles/discrete_fourier_transform.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/discrete_fourier_transform.dir/cmake_clean.cmake
.PHONY : CMakeFiles/discrete_fourier_transform.dir/clean

CMakeFiles/discrete_fourier_transform.dir/depend:
	cd /home/kai/OpenCV_tut/discrete_fourier_transform && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/kai/OpenCV_tut/discrete_fourier_transform /home/kai/OpenCV_tut/discrete_fourier_transform /home/kai/OpenCV_tut/discrete_fourier_transform /home/kai/OpenCV_tut/discrete_fourier_transform /home/kai/OpenCV_tut/discrete_fourier_transform/CMakeFiles/discrete_fourier_transform.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/discrete_fourier_transform.dir/depend

