#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv )
{
	if ( argc != 2 )
	{
		printf("usage: DisplayImage.out <Image_Path>\n");
		return -1;
	}

	Mat image;
	image = imread( argv[1], 1 );

	if ( !image.data )
	{
		printf("No image data \n");
		return -1;
	}

	namedWindow("Display Image", CV_WINDOW_AUTOSIZE );
	imshow("Original Image", image);

	Mat mtx(image);           // copy image matrix to object mtx
	//Mat kern = (Mat_<char>(3,3) << 0, -1,  0, -1,  5, -1, 0, -1,  0);

    Mat kern = (Mat_<char>(3,3) <<   1,  2,  1, 
                                     0,  0,  0, 
                                    -1, -2, -1);

    Mat image_processed;
    filter2D(image, image_processed, image.depth(), kern);

    imshow("Processed Image", image_processed);
	
    //cout << "mtx = "<< endl << " "  << mtx << endl << endl;           // output

	waitKey(0);

	return 0;
}
